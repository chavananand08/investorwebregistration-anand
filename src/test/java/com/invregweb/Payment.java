package com.invregweb;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import com.investorregmain.InvestorRegistrationWebTest;

public class Payment 
{
	public void page003Payment()
	{
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		InvestorRegistrationWebTest.driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div/div/form/div/div/ul/li[1]/div[2]/input")).sendKeys("saurabh sharma");
		InvestorRegistrationWebTest.driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div/div/form/div/div/ul/li[5]/div/button")).click();  
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		InvestorRegistrationWebTest.driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div/div/ul/li[1]/a")).click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		String winHandleBefore = InvestorRegistrationWebTest.driver.getWindowHandle();
		InvestorRegistrationWebTest.driver.switchTo().frame(0);
		InvestorRegistrationWebTest.driver.findElement(By.id("card-number")).sendKeys("4242 4242 4242 4242");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		InvestorRegistrationWebTest.driver.findElement(By.xpath("//*[@id=\"card-expiry-month\"]")).sendKeys("05");
		InvestorRegistrationWebTest.driver.findElement(By.id("card-expiry-year")).sendKeys("25");
		InvestorRegistrationWebTest.driver.findElement(By.id("card-cvv")).sendKeys("111");
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		InvestorRegistrationWebTest.driver.findElement(By.id("submit-form-button")).click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		for(String winHandle : InvestorRegistrationWebTest.driver.getWindowHandles()){
			InvestorRegistrationWebTest.driver.switchTo().window(winHandle);
		}
		System.out.println("after new window");
		InvestorRegistrationWebTest.driver.findElement(By.id("txtPassword")).sendKeys("1221");
		InvestorRegistrationWebTest.driver.findElement(By.id("cmdSubmit")).click();
		InvestorRegistrationWebTest.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// Switch back to original browser (first window)
		InvestorRegistrationWebTest.driver.switchTo().window(winHandleBefore);
	}

//	public void page003Payment() {
//		// TODO Auto-generated method stub
//		
//	}

}
